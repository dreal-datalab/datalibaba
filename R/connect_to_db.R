#' Connexion au serveur datamart
#'
#' @param db la database sur laquelle se connecter
#' @param user le profil utilisateur avec lequel se connecter : does ou dreal
#' @param user_id le nom utilisateur avec lequel se connecter tel que renseigné dans votre renviron
#' @param user_pwd le mot de passe avec lequel se connecter tel que renseigné dans votre renviron
#' @param server l'adresse ip du serveur, laisser à NULL pour utiliser le variable d'environnement du .Renviron
#' @return La fonction créé un connecteur pour se connecter à la base posgresql du serveur via le driver "PostgreSQL".
#' @importFrom DBI dbDriver dbConnect
#' @importFrom odbc odbc
#' @importFrom glue glue
#' @export
#'
#' @examples
#' \dontrun{
#' connect_to_db()
#' }
connect_to_db <- function(db="datamart",
                          user="does",
                          user_id=paste0("user_",user),
                          user_pwd = paste0("pwd_",user),
                          server = NULL){

  drv <- DBI::dbDriver("PostgreSQL")

  if(is.null(server)) {server <- Sys.getenv("server")}

  con <- DBI::dbConnect(drv,
                        dbname=db,
                        host=server,
                        port=Sys.getenv("port"),
                        user=Sys.getenv(user_id),
                        password=Sys.getenv(user_pwd)
                        )
  return(con)
}


#' Connexion au serveur via ODBC connection string
#'
#' @param db la database sur laquelle se connecter
#' @param user le profil utilisateur avec lequel se connecter : does ou dreal
#' @param user_id le nom utilisateur avec lequel se connecter tel que renseigné dans votre renviron
#' @param user_pwd le mot de passe avec lequel se connecter tel que renseigné dans votre renviron
#' @param server l'adresse ip du serveur, laisser à NULL pour utiliser le variable d'environnement du .Renviron
#' @return La fonction créé un connecteur pour se connecter à la base posgresql du serveur via le driver "PostgreSQL".
#' @return Un connecteur con
#' @importFrom DBI dbDriver dbConnect
#' @importFrom odbc odbc
#' @importFrom glue glue
#' @export
#'
#' @examples
#' \dontrun{
#' connect_to_dbi()
#' }
connect_to_dbi <- function(db="datamart",
                           user="does",
                           user_id=paste0("user_",user),
                           user_pwd = paste0("pwd_",user),
                           server = NULL){

  if(is.null(server)) {server <- Sys.getenv("server")}

  con_chaine <- glue::glue("Driver={PostgreSQL Unicode(x64)};Server=$server$;\nDatabase=$db$;\nUID=$user_id$;\nPWD=$user_pwd$;\nPort=$Sys.getenv('port')$;",
                           .open = "$",
                           .close= "$")

  con <- DBI::dbConnect(odbc::odbc(),
                        .connection_string = ,
                        timeout = 10,
                        encoding = "UTF-8")
  DBI::dbSendQuery(con,"SET client_encoding TO 'windows-1252'")
  return(con)
}
