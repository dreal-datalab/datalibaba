---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# datalibaba <img src='man/figures/logo.png' align="right" height="139" />


<!-- badges: start -->
<!-- badges: end -->

{datalibaba} est un ensemble de fonctions visant à faciliter l'alimentation du serveur de données du DREAL <datalab/> Pays de la Loire.
Site web de présentation du package : [https://dreal-datalab.gitlab.io/datalibaba/index.html](https://dreal-datalab.gitlab.io/datalibaba/index.html)

## Installation


``` r
remotes::install_gitlab('dreal-datalab/datalibaba')
```

## Example


```{r, eval=FALSE}
library(datalibaba)
```

### Vérifier la configuration de son poste

La fonction permet de s'assurer que les variables systèmes sont bien activées.

```{r, eval=FALSE}
check_server_renviron()
```

### Se connecter

con sera utilisé ensuite sur l'ensemble des autres fonctions pour dialoguer avec le serveur de base de données.

```{r, eval=FALSE}
con <- connect_to_db(db = "datamart", user = "does")
```

### Lister les schémas

```{r, eval=FALSE}
list_schemas(con)
```

### Lister les tables d'un schéma

Ici on liste les table du schéma "public"

```{r, eval=FALSE}
list_tables(con, "public")
```

### Charger une table

Un dataframe de votre environnement peut être chargée dans le serveur de base de données

```{r, eval=FALSE}
post_data(con, data = head(iris), schema = "public", table = "iris").
```

La fonction `poster_data()` se veut plus complète :   

- elle ouvre et ferme la connexion au SGBD,  
- elle gère les dataframes spatiaux, les variables temporelles et les facteurs,  
- elle adapte le dataset posté pour respecter quelques contraintes d'administration du SGBD (normalisation du nom des variables, ajout de clé primaire, et pour les tables spatiales : ajout d'index spatial, déclaration du CRS et du nb de dimensions de la géométrie)  
- poste quelques métadonnées en commentaire de la table (auteur du chargement et sa date)  
- ajoute les droits de lecture/écriture du schéma à la table postée.   

Le schéma spécifié en argument est créé s'il n'existe pas. 

```{r, eval=FALSE}
poster_data(data = iris, table = "test_iris", schema = "public", db = "public", user = "does", overwrite = TRUE)
```

### Télécharger une table

Une table du serveur de base de données peut être téléchargée en dataframe.

```{r, eval=FALSE}
db_iris <- get_data(con, schema = "public", table = "iris")
```

La fonction `importer_data()` se veut plus complète :  

- elle ouvre et ferme la connexion au SGBD, 
- elle gère les dataframes spatiaux, les variables temporelles et les facteurs, 
- et 'déstandardise' les noms de champs si le dataset a été posté avec `poster_data()`,

de manière à retrouver le dataframe dans l'état précis où il était avant son versement au SGBD.


```{r, eval=FALSE}
db_iris <- importer_data(table = "test_iris", schema = "public", db = "public", user = "does", overwrite = TRUE)
```

### Poster des métadonnées en commentaires   

On peut commenter les tables chargées sur le SGBD depuis RStudio :

```{r, eval=FALSE}
commenter_table(comment = "Je suis un commentaire important sur la table test_iris.", table = "test_iris", 
                schema = "public", db = "public", user = "does")
```
En dehors des informations spécifiées au niveau de l'argument _comment_, la fonction ajoute le nom de l'auteur et la date du commentaire. 

Des fonctions existent également pour commenter un schéma : `commenter_schema()` ou un champ de table (= une variable ou un attribut) : `commenter_champ()`.

Enfin la fonction `post_dico_attr()` permet de verser tout un dictionnaire d'attributs/dictionnaire des variables en commentaires de champ à partir d'un dataframe. 
Ce dataframe doit comprendre deux champs : le 1er pour le nom des variables, le second pour leur explicitation. Le nommage de ces deux champs n'a pas d'importance, leur ordre importe en revanche. 
Ce dataframe peut comprendre autant de lignes que d'attributs que l'on souhaite documenter.  

```{r, eval=FALSE}
dico_iris <- data.frame(VAR = names(iris), VAR_LIB = paste("ceci est le libellé descriptif de", names(iris)))
post_dico_attr(dico = dico_iris, table = "test_iris", schema = "public", db = "public", user = "does")
```

### Récupérer des commentaires de tables SGBD

On peut récupérer les commentaires de tables chargées sur le SGBD dans RStudio :

```{r, eval=FALSE}
dico_var <- get_table_comments(table = "test_iris", schema = "public", db = "public", user = "does")
```

On récupère à la fois les commentaires de champs et le commentaire de la table.


